# MIT License
# 
# Copyright (c) 2019 Gustavo Moitinho Trindade
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import pygame
from spaceship import Spaceship
from moon import MoonSurface

MOON_SECTION_NUMBER = 800
MOON_SECTION_WIDTH = 60

SPACESHIP_INITIAL_HEIGHT = -3000
CLOSEUP_DISTANCE=400

FAR_SCALE = 1/8
CLOSUP_SCALE = 1
SPRITE_FAR_SCALE = 5/8
SPRITE_CLOSUP_SCALE = 1

METRES_PER_PIXEL = 1/4

BLACK = (0x00, 0x00, 0x00)
WHITE = (0xff, 0xff, 0xff)

BACKGROUND_COLOR = BLACK

WIDTH = 1000
HEIGHT = 600

FRAME_FREQUENCY = 60

FONT_SIZE = 16

def main():
    pygame.init()
    pygame.display.set_caption('Moonlander')

    clock = pygame.time.Clock()

    font = pygame.font.Font("assets/fonts/Inconsolata.otf", FONT_SIZE)

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    camera = [0, 0]
    got_close_to_moon = False

    spaceship = Spaceship(((MOON_SECTION_NUMBER//2)*MOON_SECTION_WIDTH, SPACESHIP_INITIAL_HEIGHT), metres_per_pixel=METRES_PER_PIXEL)
    spaceship.set_horizontal_velocity(30)
    spaceship.set_rotation(70)

    scale = FAR_SCALE
    spaceship.set_sprite_scale(SPRITE_FAR_SCALE)
    spaceship.set_scale(scale)
    moon_surface = MoonSurface(MOON_SECTION_NUMBER, MOON_SECTION_WIDTH)

    player_input = {'left': False, 'right': False, 'rocket': False}

    landed = False

    running = True
    # Main loop
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                print('Exiting...')
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    print('Exiting...')
                if event.key == pygame.K_LEFT:
                    player_input['left'] = True
                if event.key == pygame.K_RIGHT:
                    player_input['right'] = True
                if event.key == pygame.K_UP:
                    player_input['rocket'] = True
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    player_input['left'] = False
                if event.key == pygame.K_RIGHT:
                    player_input['right'] = False
                if event.key == pygame.K_UP:
                    player_input['rocket'] = False

        # Clear screen
        screen.fill(BACKGROUND_COLOR)

        # Test if player got close to moon
        if not got_close_to_moon:
            if moon_surface.is_in_range(spaceship, CLOSEUP_DISTANCE):
                got_close_to_moon = True
                spaceship.set_sprite_scale(1.0)
                scale = CLOSUP_SCALE
                spaceship.set_scale(scale)

        # Sets camera to player position
        if got_close_to_moon:
            camera[0] = spaceship.get_position()[0] - WIDTH//2
            camera[1] = spaceship.get_position()[1] - HEIGHT//2
        else:
            camera[0] = spaceship.get_position()[0]*scale - WIDTH//2
            camera[1] = 0

        if not landed and not spaceship.is_destroyed():
            # Sets the inputs, it eats them
            spaceship.set_input(player_input)
            # At every gametick, update the spaceship
            spaceship.tick()

            # Moon surface collision testing
            landed = moon_surface.spaceship_collision(spaceship, debug_surface=screen, debug_camera=camera)

        # Draws spaceship
        if not spaceship.is_destroyed():
            spaceship.draw_on(screen, camera)

        # Draw moon surface
        # The reason for this weird "HEIGHT - ((HEIGHT ...", is that the Y axis is upside down, whilst
        # the game, the player, and everyone else actually, considers height to increase as things go UP.
        # Therefore, some things had to be done to make up still be up when scaling.
        moon_surface_points = [(p[0]*scale - camera[0], HEIGHT - ((HEIGHT - p[1])*scale) - camera[1]) for p in moon_surface.get_surface_points()]
        pygame.draw.aalines(screen, WHITE, False, moon_surface_points)

        # Draw HUE
        horizontal_velocity_text = font.render('HORIZONTAL VELOCITY: {0:+.6f}'.format(spaceship.get_horizontal_velocity()), True, WHITE)
        vertical_velocity_text   = font.render('VERTICAL VELOCITY:   {0:+.6f}'.format(spaceship.get_vertical_velocity()), True, WHITE)
        screen.blit(horizontal_velocity_text, (10, 10))
        screen.blit(vertical_velocity_text, (10, 30))

        #pygame.draw.polygon(screen, (255, 0, 0), spaceship.get_ship_polygon(), 1)
        #print('\t{}'.format(spaceship.get_ship_polygon()))

        # Swap buffers
        pygame.display.flip()

        # Game ticks
        clock.tick(FRAME_FREQUENCY)

main();
