# MIT License
# 
# Copyright (c) 2019 Gustavo Moitinho Trindade
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import pygame
from shapely.geometry import Polygon

LONG_VERTICAL_DIST = 100
LONG_HORIZONTAL_DIST = 200

SPACESHIP_ANGLE_MARGIN = 3.0
SPACESHIP_VERTICAL_VELOCITY_MARGIN = 10.0
SPACESHIP_HORIZONTAL_VELOCITY_MARGIN = 5.0

class MoonSurface:
    def __init__(self, number_of_sections, lengh_of_section):
        self.subdivisions = number_of_sections
        self.width = lengh_of_section * number_of_sections
        self.length_of_section = lengh_of_section

        self.points = [(x*self.length_of_section, 0) for x in range(0, self.subdivisions + 1)]

        make_into_moon_surface_linear(self.points, self.length_of_section)
        #print(self.points)

    def get_surface_points(self):
        return self.points

    def __spaceship_get_proximity(self, spaceship):
        # Use this X to decide the section to test
        x_position = spaceship.get_position()[0]
        lo_x = x_position - LONG_HORIZONTAL_DIST
        hi_x = x_position + LONG_HORIZONTAL_DIST
        lo_index = max(0, int(lo_x/self.length_of_section))
        hi_index = min(self.subdivisions, int(hi_x/self.length_of_section))

        return lo_index, hi_index

    def spaceship_collision(self, spaceship, debug_surface=None, debug_camera=(0, 0)):
        # If collides
        if self.spaceship_collision_test(spaceship, debug_surface=debug_surface, debug_camera=debug_camera):
            print('Collided')
            angle = spaceship.get_rotation()
            # Check if spaceship feet are perpendicular to gravity
            if (
                (abs(angle) < SPACESHIP_ANGLE_MARGIN) and
                (abs(spaceship.get_horizontal_velocity()) < SPACESHIP_HORIZONTAL_VELOCITY_MARGIN) and
                (abs(spaceship.get_vertical_velocity()) < SPACESHIP_VERTICAL_VELOCITY_MARGIN)
            ):
                print('Angle and speed OK')
                (x, y) = spaceship.get_position()
                width, height = spaceship.get_dimensions()
                left_foot = x - width//2
                right_foot = x + width//2

                # Check bounds
                if left_foot > 0 and right_foot < self.width:
                    print('Feet in range')
                    # Get the surface line under each feet
                    left_surface_index = int(left_foot/self.length_of_section)
                    right_surface_index = int(right_foot/self.length_of_section)
                    print(left_surface_index, right_surface_index)

                    left_foot_line = (self.points[left_surface_index], self.points[left_surface_index + 1])
                    right_foot_line = (self.points[right_surface_index], self.points[right_surface_index + 1])

                    #print(left_foot_line)
                    #print(right_foot_line)

                    if (
                        (abs(left_foot_line[0][1] - left_foot_line[1][1]) < SPACESHIP_ANGLE_MARGIN) and
                        (abs(left_foot_line[1][1] - right_foot_line[0][1]) < SPACESHIP_ANGLE_MARGIN) and
                        (abs(right_foot_line[0][1] - right_foot_line[1][1]) < SPACESHIP_ANGLE_MARGIN)
                    ):
                        print('Feet alligned')
                        # Avoid placing ship inside the moon
                        spaceship.set_position((x, left_foot_line[0][1] - height//2))
                        return True

            # If did not return previously there
            spaceship.destroy()

        return False


    def spaceship_collision_test(self, spaceship, debug_surface=None, debug_camera=(0, 0)):
        lo_index, hi_index = self.__spaceship_get_proximity(spaceship)

        if lo_index >= hi_index:
            return False

        # Convert to shapely Polygon
        ship_polygon = Polygon(spaceship.get_ship_polygon())

        index = lo_index

        line = (self.points[index], self.points[index])
        # For each line on the moon's surface
        while index < hi_index + 1:
            line = (line[1], self.points[index])

            (p0, p1) = line
            polypoints = [p0, p1, (p1[0], p1[1] + LONG_VERTICAL_DIST), (p0[0], p0[1] + LONG_VERTICAL_DIST)]
            moon_semi_poly = Polygon(polypoints)

            if debug_surface is not None:
                debug_points = [(p[0] - debug_camera[0], p[1] - debug_camera[1]) for p in polypoints]
                #print(debug_points)
                #print(spaceship.get_position())
                pygame.draw.polygon(debug_surface, (0, 0, 255), debug_points, 1)

            if (moon_semi_poly.intersects(ship_polygon)):
                return True

            index += 1

        return False

    def is_in_range(self, spaceship, dist):
        lo_index, hi_index = self.__spaceship_get_proximity(spaceship)
        y_position = spaceship.get_position()[1]

        if lo_index >= hi_index:
            return False

        for (_, y_moon) in self.points[lo_index:hi_index + 1]:
            if y_moon - y_position < dist:
                return True

        return False

import random

MOUNTAIN_UP_CHANCE = 6
MOUNTAIN_DOWN_CHANCE = 6

MOUNTAIN_MIN_COUNTER = 3
MOUNTAIN_MAX_COUNTER = 12

MAX_HEIGHT = 2500
MIN_HEIGHT = 0

SMOOTH_OFFSET_RADIUS = 36
SMOOTH_OFFSET_CHANCE = 10
SMOOTH_OFFSET_STEP = 6

def make_into_moon_surface_linear(points, length_of_section):
    mountain = 0
    level = 0
    for i in range(0, len(points)):
        offset = 0
        if mountain > 0:
            offset = random.uniform(-0.5, 2.5)*length_of_section
            mountain -= 1
        elif mountain < 0:
            offset = random.uniform(-2.5, 0.5)*length_of_section
            mountain += 1
        else:
            # 90% chance of smoothing
            if random.randint(0, 100) > SMOOTH_OFFSET_CHANCE:
                # Calculate smooth offset
                offset = random.randrange(-SMOOTH_OFFSET_RADIUS, SMOOTH_OFFSET_RADIUS, SMOOTH_OFFSET_STEP)
            # X% chance of going way up on next iterations
            if random.randint(0, 100) < MOUNTAIN_UP_CHANCE:
                mountain = random.randint(MOUNTAIN_MIN_COUNTER, MOUNTAIN_MAX_COUNTER)
            # Y% chance of going way down on next iterations
            if random.randint(0, 100) < MOUNTAIN_DOWN_CHANCE:
                mountain = -random.randint(MOUNTAIN_MIN_COUNTER, MOUNTAIN_MAX_COUNTER)

        if abs(offset) < SMOOTH_OFFSET_STEP and abs(offset) != 0.0:
            offset = SMOOTH_OFFSET_RADIUS*(offset/abs(offset))
        level += offset

        # If too low, start new mountain instead
        if level < MIN_HEIGHT:
            level = 0
            mountain = random.randint(1, 3)
        # If too high, descent on a cliff
        if level > MAX_HEIGHT:
            level = 0
            mountain = -random.randint(1, 3)

        points[i] = (points[i][0], -level)
