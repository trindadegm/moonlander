# MIT License
# 
# Copyright (c) 2019 Gustavo Moitinho Trindade
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import pygame
import math

GRAVITY = 1.6
ROCKET_ACCELERATION = 3.6
ROTATION_RATIO = 90.0

ELAPSED_TIME = 1/60

class Spaceship:
    def __init__(self, initial_pos=(0, 0), metres_per_pixel=1/4):
        self.spaceship_sprite_unscaled = pygame.image.load('assets/sprites/ship.png')
        self.spacefire_sprite_unscaled = pygame.image.load('assets/sprites/fire.png')

        self.spaceship_sprite = self.spaceship_sprite_unscaled.copy()
        self.spacefire_sprite = self.spacefire_sprite_unscaled.copy()

        self.position = (initial_pos[0], initial_pos[1])
        self.rotation = 0

        self.metres_per_pixel = metres_per_pixel

        self.horizontal_velocity = 0.0
        self.vertical_velocity = 0.0

        self.sprite_scale = 1.0
        self.scale = 1/4

        self.player_input = {'left': False, 'right': False, 'rocket': False}

        self.tick_counter = 0
        self.destroyed = False

    def set_sprite_scale(self, scale):
        self.sprite_scale = scale

    def set_scale(self, scale):
        self.scale = scale

    def get_ship_polygon(self):
        #return self.position + (self.spaceship_sprite.get_width(), self.spaceship_sprite.get_height())
        width = self.spaceship_sprite.get_width()
        height = self.spaceship_sprite.get_height()
        angle = -math.radians(self.rotation)

        polygon = [(-width//2, -height//2), (width//2, -height//2), (width//2, height//2), (-width//2, height//2)]
        new_polygon = []
        for (x, y) in polygon:
            new_point = (
                    (x*math.cos(angle) - y*math.sin(angle)) + self.position[0],
                (x*math.sin(angle) + y*math.cos(angle)) + self.position[1]
            )
            new_polygon.append(new_point)

        return new_polygon

    def set_position(self, pos):
        self.position = pos

    def get_position(self):
        return self.position

    def get_dimensions(self):
        return (self.spaceship_sprite.get_width(), self.spaceship_sprite.get_height())

    def set_rotation(self, angle):
        self.rotation = angle

    def get_rotation(self):
        return self.rotation

    def set_horizontal_velocity(self, v):
        self.horizontal_velocity = v

    def get_horizontal_velocity(self):
        return self.horizontal_velocity

    def set_vertical_velocity(self, v):
        self.vertical_velocity = v

    def get_vertical_velocity(self):
        return self.vertical_velocity

    def draw_on(self, screen, camera=(0, 0)):
        # Display size
        display_w, display_h = pygame.display.get_surface().get_size()
        # Rotate sprite
        rotated_spaceship_sprite = pygame.transform.rotozoom(self.spaceship_sprite, self.rotation, self.sprite_scale)
        center_position = rotated_spaceship_sprite.get_rect().center
        pos_x = self.position[0]*self.scale
        pos_y = display_h - ((display_h - self.position[1])*self.scale)
        draw_spaceship_position = (pos_x - center_position[0] - camera[0], pos_y - center_position[1] - camera[1])

        screen.blit(rotated_spaceship_sprite, draw_spaceship_position)

    def set_input(self, player_input):
        self.player_input = player_input

    def tick(self):
        if self.destroyed:
            return

        #print('\t{} {}'.format(self.horizontal_velocity, self.vertical_velocity))
        self.tick_counter += 1

        # Update position
        self.position = (self.position[0] + self.metres_per_pixel*self.horizontal_velocity, self.position[1] + self.metres_per_pixel*self.vertical_velocity)

        # Update velocity with gravity
        self.vertical_velocity += ELAPSED_TIME*GRAVITY

        if self.player_input['rocket']:
            # Calculate direction of rocket
            rocket_angle = math.radians(self.rotation - 90.0)
            horizontal_factor = math.cos(rocket_angle)
            vertical_factor = math.sin(rocket_angle)
            # Update velocity with rocket
            self.vertical_velocity += ELAPSED_TIME*vertical_factor*ROCKET_ACCELERATION
            self.horizontal_velocity -= ELAPSED_TIME*horizontal_factor*ROCKET_ACCELERATION
        if self.player_input['left']:
            self.rotation += ELAPSED_TIME*ROTATION_RATIO
        if self.player_input['right']:
            self.rotation -= ELAPSED_TIME*ROTATION_RATIO

        self.rotation = min(self.rotation, 90)
        self.rotation = max(self.rotation, -90)

    def destroy(self):
        self.destroyed = True

    def is_destroyed(self):
        return self.destroyed
