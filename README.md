# moonlander

Simple Python Moonlander clone

# How to run
Make sure you have Python 3.7 installed.

## Install the requirements
This application requires pygame and Shapely.
You can install them with pip:
```
% pip install -r requirements.txt
```

## Run the application
```
% python moonlander.py
```

## Notes
`python` and `pip` may have to be replaced with `python3` and `pip3` on some systems.

# Licensing
This project is licensed under the MIT license. The license can be found on the file `LICENSE`.

# Fonts
The font used on the game is Inconsolata, by Raph Levien. The license of the font is on the file `assets/fonts/OFL.txt`.
